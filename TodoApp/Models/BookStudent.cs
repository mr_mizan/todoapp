﻿namespace TodoApp.Models
{
    public class BookStudent
    {
        public int BookId { get; set; }
        public Book? Books { get; set; }
        public int StudentId { get; set; }
        public Student? Students { get; set; }

    }
}
