﻿using Microsoft.EntityFrameworkCore;

namespace TodoApp.Models
{
    public class AppDbContect : DbContext
    {
        public AppDbContect(DbContextOptions options) : base(options)
        {
        }
        public virtual DbSet<Student> Students { get; set; } = default;
        public virtual DbSet<Department> Departments { get; set; } = default;
        public virtual DbSet<Book> Books { get; set; } = default;
        public virtual DbSet<BookStudent> BookStudents { get; set; } = default;

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
         modelBuilder.Entity<Department>().HasMany(e=> e.Students).WithOne(e => e.Department).HasForeignKey(e => e.DepartmentId);
            modelBuilder.Entity<BookStudent>().HasKey(x => new { x.BookId, x.StudentId });

            modelBuilder.Entity<BookStudent>().HasOne(x => x.Books)
                                              .WithMany(x => x.Students)
                                              .HasForeignKey(x => x.StudentId);
            modelBuilder.Entity<BookStudent>().HasOne(x => x.Students)
                                                  .WithMany(x => x.Books)
                                                  .HasForeignKey(x => x.BookId);

        }
    }

}
