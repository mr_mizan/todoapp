﻿using System.ComponentModel.DataAnnotations;

namespace TodoApp.Models
{
    public class Student
    {
        [Key]
        public int Id { get; set; }
        [Required]
        [StringLength(10)]
        public string Name { get; set; } = default;
        [Required]
        [StringLength(100)]
        public string details { get; set; } = default;
        public Boolean IsActive { get; set; }
        public int DepartmentId { get; set; }
        public Department Department { get; set; }
        public List<BookStudent> Books { get; set; }

    }
}
