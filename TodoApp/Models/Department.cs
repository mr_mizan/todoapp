﻿using System.ComponentModel.DataAnnotations;

namespace TodoApp.Models
{
    public class Department
    {

        [Key]
        public int Id { get; set; }
        [Required]
        [StringLength(10)]
        public string Name { get; set; } = default;
        public List<Student> Students { get; set; }
    }
}
